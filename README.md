ACP TIME CALCULATOR (Project 5 - CIS 322)

This tool calculates the ACP-sanctioned brevets between 200 and 1000 kilometers.

As the user, you may input different control points (a distance in either miles or kilometers) throughout a brevet, and in return you will receive the opening and closing times defined by RUSA (Randonneurs USA). 

The opening and closing times are provided in 24 hour formats. 

For a better understanding of what is going down under the hood, algorithmically, you can refer to the RUSA.org's FAQ: https://rusa.org/pages/acp-brevet-control-times-calculator.

STEPS TO OPERATE THE CALCULATOR:

1. clone this project repo onto your desktop's filesystem
2. download DOCKER via docker.com
3. cd DockerMongo
4. build the docker image
5. access the calculator via your web browser at: http://127.0.0.1:5000/
6. Update the form with your control points in either miles or kilometers
7. Submit your updates using the 'submit' button
8. Preview your score sheet using the 'display' button

Implemented by: Kelemen Szimonisz (kelemens@uoregon.edu)

Project prompt and start code provided by: University of Oregon CIS 322 SPRING 2019
